#ifndef _QRCODE_H_
#define _QRCODE_H_

#define VERSION "0.2"
#define __STATIC static

#define INCHES_PER_METER (100.0 / 2.54)
#define MAX_DATA_SIZE (7090 * 16) // from specification

static int writePNG(QRcode *, const char *);
static int writeGIF(QRcode *, const char *);
static int writeSVG(QRcode *, const char *);
static int writePDF(QRcode *, const char *);
static int writePS(QRcode *, const char *);
static int writeX(QRcode *, const char *);
static int writeASCII(QRcode *, const char *);

#endif /* _QRCODE_H_ */
