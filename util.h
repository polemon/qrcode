#ifndef _UTIL_H_
#define _UTIL_H_

static int Util_computeMinMQRversion(int, char *);

static int Util_checkModeNum(int, char *);
static int Util_checkModeAlnum(int, char *);
static int Util_checkModeBin(int, char *);
static int Util_checkModeKanji(int, char *);

static int Util_computeBitsNum(int);
static int Util_computeBitsAlnum(int);
static int Util_computeBitsBin(int);
static int Util_computeBitsKanji(int);

#define Util_checkAlnum(__c__) ( \
    (__c__ & 0x80) ? -1 : Util_QRalnum[(int)__c__] \
)

#endif /* _UTIL_H_ */
