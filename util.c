
#include "util.h"

static signed char Util_MQRcappNum[4][4] = {
//   M1 M2  M3  M4
    {5, 10, 23, 35}, // L
    {0,  8, 18, 30}, // M    
    {0,  0,  0, 21}, // Q
    {0,  0,  0,  0}  // H
};

static signed char Util_MQRcappAlnum[4][4] = {
//   M1 M2  M3  M4
    {0,  6, 14, 21}, // L
    {0,  5, 11, 18}, // M    
    {0,  0,  0, 13}, // Q
    {0,  0,  0,  0}  // H
};

static signed char Util_MQRcappBin[4][4] = {
//   M1 M2  M3  M4
    {0,  0,  9, 15}, // L
    {0,  0,  7, 13}, // M    
    {0,  0,  0,  9}, // Q
    {0,  0,  0,  0}  // H
};

static signed char Util_MQRcappKanji[4][4] = {
//   M1 M2  M3  M4
    {0,  0,  6,  9}, // L
    {0,  0,  4,  8}, // M    
    {0,  0,  0,  5}, // Q
    {0,  0,  0,  0}  // H
};

/* data bits capacity for version and ECC level */
static signed char Util_MQRcappBits[4][4] = {
//   M1  M2  M3  M4
    {20, 40, 84, 128}, // L
    { 0, 32, 68, 112}, // M
    { 0,  0,  0,  80}, // Q
    { 0,  0,  0,   0}  // H
};

/*
 * Numeric Values
 */

static int Util_checkModeNum(int size, char *data) {
    int i;

    for(i = 0; i < size; i++)
        if(data[i] < '0' || data[i] > '9')
            return 0;

    return 1;
}

static int Util_computeBitsNum(int size) {
    int w, bits;

    w = size / 3;
    bits = w * 10;

    switch(size - w * 3) {
        case 1:
            bits += 4;
            break;
        case 2:
            bits += 7;
            break;
        default:
            break;
    }

    return bits;
}

/*
 * Alphanumeric Values
 */

// "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:"
const signed char Util_QRalnum[128] = {
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43,
     0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 44, -1, -1, -1, -1, -1,
    -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
    25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};

static int Util_checkModeAlnum(int size, char *data) {
    int i;

    for(i = 0; i < size; i++) 
        if(Util_checkAlnum(data[i]) < 0)
            return 0;

    return 1;
}

static int Util_computeBitsAlnum(int size) {
    int w, bits;

    w = size / 2;
    bits = w * 11;

    if(size & 1)
        bits += 6;

    return bits;
}


/*
 * Binary Values
 */

static int Util_checkModeBin(int size, char *data) {
    return 1;
}

static int Util_computeBitsBin(int size) {
    return size * 8;
}

/*
 * Kanji Values
 */

static int Util_checkModeKanji(int size, char *data) {
    int i;
    unsigned int val;

    if(size & 1)
        return 0;

    for(i = 0; i < size; i += 2) {
        val = ((unsigned int)data[i] << 8) | data[i +1];

        if(val < 0x8140 || (val > 0x9ffc && val < 0xe040) || val > 0xebbf)
            return 0;
    }

    return 1;
}

static int Util_computeBitsKanji(int size) {
    return (size / 2) * 13;
}

/*
 * Fitting Version
 * XXX This shouldn't be confused with 'optimal'!
 * In fact, this process is _not_ optimal, it just seeks a workable version.
 */

static int Util_computeMinMQRversion(int size, char *data) {


    return 0;
}

