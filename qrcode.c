/**
 * qrcode - QR Code and Micro QR Code generator.
 *
 * Copyright (c) 2011 - 2012, Szymon 'polemon' Bereziak <polemon@polemon.org>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cairo/cairo.h>
#include <cairo/cairo-pdf.h>
#include <cairo/cairo-ps.h>
#include <cairo/cairo-xlib.h>
#include <png.h>
#include <gd.h>
#include <getopt.h>
#include <qrencode.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>

#include "util.h"
#include "qrcode.h"

/*
 * running modes / program options
 */

// general rendering options
static int casesensitive = 1;
static int eightbit      = 0;
static int version       = 0;
static int size          = 3;
static int margin        = -1;
static int structured    = 0;
static int transparent   = 0;
static int micro         = 0;
static int dpi           = 72; // this thing is only used by libpng, no point in setting it somewhere else.
static QRecLevel level   = QR_ECLEVEL_L;
static QRencodeMode hint = QR_MODE_8;

// internal rendering settings
static int eps           = 0;

// function pointer to selected rendering method (default = PNG)
int (*writeF)(QRcode *, const char *) = &writePNG;

// cli switches
static const struct option options[] = {
    {"help"         , no_argument      , NULL, 'h'},
    {"output"       , required_argument, NULL, 'o'},
    {"format"       , required_argument, NULL, 'f'},
//  {"dpi"          , required_argument, NULL, 'd'},
    {"micro"        , no_argument      , NULL, 'M'},
    {"level"        , required_argument, NULL, 'l'},
    {"size"         , required_argument, NULL, 's'},
    {"transparent"  , no_argument      , NULL, 't'},
    {"symversion"   , required_argument, NULL, 'v'},
    {"margin"       , required_argument, NULL, 'm'},
    {"structured"   , no_argument      , NULL, 'S'},
    {"kanji"        , no_argument      , NULL, 'k'},
    {"casesensitive", no_argument      , NULL, 'c'},
    {"ignorecase"   , no_argument      , NULL, 'i'},
    {"8bit"         , no_argument      , NULL, '8'},
    {"version"      , no_argument      , NULL, 'V'},
    {NULL, 0, NULL, 0}
};

static char *optstring = "ho:f:d:l:s:tv:m:MSkci8V";

/*
 * help options / usage
 */

static void usage(int help, int longopt) {
    fprintf(stderr,
            "qrcode version %s\n"
            "Copyright (C) 2011 - 2012 Szymon 'polemon' Bereziak\n", VERSION);
    if(help) {
        if(longopt) {
            fprintf(stderr,
                    "Usage: qrencode [OPTION]... [STRING]\n"
                    "Encode input data in a QR Code and save as a PNG image.\n\n"
                    "  -h, --help   display the help message. -h displays only the help of short\n"
                    "               options.\n\n"
                    "  -o FILENAME, --output=FILENAME\n"
                    "               write PNG image to FILENAME. If '-' is specified, the result\n"
                    "               will be output to standard output. If -S is given, structured\n"
                    "               symbols are written to FILENAME-01.png, FILENAME-02.png, ...;\n"
                    "               if specified, remove a trailing '.png' from FILENAME.\n\n"
                    "  -f FORMAT, --format=FORMAT\n"
                    "               select output format: PNG/GIF/SVG/PDF/PS/EPS/ASCII. (default=PNG)\n\n"
                    "  -s NUMBER, --size=NUMBER\n"
                    "               specify the size of dot (pixel (PNG/GIF)\n"
                    "               or point (SVG/PDF/PS/EPS)). (default=3)\n\n"
//                  "  -d NUMBER, --dpi=NUMBER\n"
//                  "               specify the DPI of generated PNG. (default=72)\n\n"
                    "  -t, --transparent\n"
                    "               make background transparent.\n\n"
                    "  -M, --micro\n"
                    "               encode in a Micro QR code.\n\n"
                    "  -l {LMQH}, --level={LMQH}\n"
                    "               specify error correction level from L (lowest) to H (highest).\n"
                    "               (default=L)\n\n"
                    "  -v NUMBER, --symversion=NUMBER\n"
                    "               specify the version of the symbol. (default=auto)\n\n"
                    "  -m NUMBER, --margin=NUMBER\n"
                    "               specify the width of margin. (default=4)\n\n"
                    "  -S, --structured\n"
                    "               make structured symbols. Version must be specified.\n\n"
                    "  -k, --kanji  assume that the input text contains kanji (shift-jis).\n\n"
                    "  -c, --casesensitive\n"
                    "               encode lower-case alphabet characters in 8-bit mode. (default)\n\n"
                    "  -i, --ignorecase\n"
                    "               ignore case distinctions and use only upper-case characters.\n\n"
                    "  -8, -8bit    encode entire data in 8-bit mode. -k, -c and -i will be ignored.\n\n"
                    "  -V, --version\n"
                    "               display the version number and copyrights of the qrencode.\n\n"
                    "  [STRING]     input data. If it is not specified, data will be taken from\n"
                    "               standard input.\n"
                    );
        } else {
            fprintf(stderr,
                    "Usage: qrencode [OPTION]... [STRING]\n"
                    "Encode input data in a QR Code and save as a PNG image.\n\n"
                    "  -h           display this message.\n"
                    "  --help       display the usage of long options.\n"
                    "  -o FILENAME  write PNG image to FILENAME. If '-' is specified, the result\n"
                    "               will be output to standard output. If -S is given, structured\n"
                    "               symbols are written to FILENAME-01.png, FILENAME-02.png, ...;\n"
                    "               if specified, remove a trailing '.png' from FILENAME.\n"
                    "  -f FORMAT    select output format: PNG/GIF/SVG/PDF/PS/EPS/ASCII. (default=PNG)\n"
                    "  -s NUMBER    specify the size of dot (pixel (PNG/GIF)\n"
                    "               or points (SVG/PDF/PS/EPS)). (default=3)\n"
//                  "  -d NUMBER    specify the DPI of PNG. (default=72)\n"
                    "  -t           make background transparent.\n"
                    "  -M           encode in a Micro QR Code.\n"
                    "  -l {LMQH}    specify error correctionlevel from L (lowest) to H (highest).\n"
                    "               (default=L)\n"
                    "  -v NUMBER    specify the version of the symbol. (default=auto)\n"
                    "  -m NUMBER    specify the width of margin. (default=4)\n"
                    "  -S           make structured symbols. Version must be specified.\n"
                    "  -k           assume that the input text contains kanji (shift-jis).\n"
                    "  -c           encode lower-case alphabet characters in 8-bit mode. (default)\n"
                    "  -i           ignore case distinctions and use only upper-case characters.\n"
                    "  -8           encode entire data in 8-bit mode. -k, -c and -i will be ignored.\n"
                    "  -V           display the version number and copyrights of the qrencode.\n"
                    "  [STRING]     input data. If it is not specified, data will be taken from\n"
                    "               standard input.\n"
                    );
        }
    }
}

// read buffered stdio
static char *readStdin(int *length) {
    char *buffer;
    int ret;

    buffer = (char *)malloc(MAX_DATA_SIZE);
    if(buffer == NULL) {
        fprintf(stderr, "Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }
    ret = fread(buffer, 1, MAX_DATA_SIZE, stdin);
    if(ret == 0) {
        fprintf(stderr, "No input data.\n");
        exit(EXIT_FAILURE);
    }
    if(feof(stdin) == 0) {
        fprintf(stderr, "Input data is too large.\n");
        exit(EXIT_FAILURE);
    }

    buffer[ret] = '\0';
    *length = ret;

    return buffer;
}

// write handle open.
static FILE *openFile(const char *outfile) {
    FILE *fp;

    if(outfile[0] == '-' && outfile[1] == '\0') {
        fp = stdout;
    } else {
        fp = fopen(outfile, "wb");
        if(fp == NULL) {
            fprintf(stderr, "Failed to create file: %s\n", outfile);
            perror(NULL);
            exit(EXIT_FAILURE);
        }
    }

    return fp;
}

/*
 * Cairo member functions
 */

// file write handler (always returns success, since fopen() sanity is guaranteed elsewhere).
static cairo_status_t cairowrite(void *fh, unsigned char const *data, unsigned int length) {
    fwrite(data, length, 1, (FILE *)fh);
    return CAIRO_STATUS_SUCCESS;
}

// drawing function on cairo surface
static void cairodraw(QRcode *qrcode, cairo_surface_t *surface, double realwidth) {
    int bar, blocklength;
    int x, y, xs;
    unsigned char *p;
    cairo_t *context;

    context = cairo_create(surface);

    if(!transparent) {
        cairo_rectangle(context, 0.0, 0.0, realwidth, realwidth);
        cairo_set_source_rgb(context, 1.0, 1.0, 1.0);
        cairo_fill(context);
    }

    cairo_set_source_rgb(context, 0.0, 0.0, 0.0);

    p = qrcode->data;
    for(y = 0; y < qrcode->width; y++) {
        blocklength = 1;
        bar = 0;

        for(x = 0; x < qrcode->width; x++) {
            if(!bar && (*p & 1)) {
                xs = x;
                bar = 1;
                blocklength = 1;
            } else if(bar && (*p & 1)) {
                blocklength++; 
            } else if(bar && !(*p & 1)) {
                cairo_rectangle(context, (double)(xs + margin) * size, (double)(y + margin) * size, (double)size * blocklength, (double)size);
                cairo_fill(context);
                bar = 0;
            }

            p++;
        }

        if(bar) {
            cairo_rectangle(context, (double)(xs + margin) * size, (double)(y + margin) * size, (double)size * blocklength, (double)size);
            cairo_fill(context);
        }
    }

    cairo_show_page(context);
    cairo_destroy(context);

    return;
}

/*
 * Rendering functions
 */

// PNG rendering
static int writePNG(QRcode *qrcode, const char *outfile) {
    static FILE *fp; // avoid clobbering by setjmp.
    png_structp png_ptr;
    png_infop info_ptr;
    png_color palette[2];
    png_byte trans_alpha[2];
    unsigned char *row, *p, *q;
    int x, y, xx, yy, bit;
    int realwidth;

    realwidth = (qrcode->width + margin * 2) * size;
    row = (unsigned char *)malloc((realwidth + 7) / 8);
    if(row == NULL) {
        fprintf(stderr, "Failed to allocate memory.\n");
        exit(EXIT_FAILURE);
    }

    fp = openFile(outfile);

    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if(png_ptr == NULL) {
        fprintf(stderr, "Failed to initialize PNG writer.\n");
        exit(EXIT_FAILURE);
    }

    info_ptr = png_create_info_struct(png_ptr);
    if(info_ptr == NULL) {
        fprintf(stderr, "Failed to initialize PNG write.\n");
        exit(EXIT_FAILURE);
    }

    if(setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_write_struct(&png_ptr, &info_ptr);
        fprintf(stderr, "Failed to write PNG image.\n");
        exit(EXIT_FAILURE);
    }

    png_init_io(png_ptr, fp);
    png_set_IHDR(png_ptr, info_ptr,
            realwidth, realwidth,
            1,
            PNG_COLOR_TYPE_PALETTE,
            PNG_INTERLACE_NONE,
            PNG_COMPRESSION_TYPE_DEFAULT,
            PNG_FILTER_TYPE_DEFAULT);

    png_set_pHYs(png_ptr, info_ptr,
            dpi * INCHES_PER_METER,
            dpi * INCHES_PER_METER,
            PNG_RESOLUTION_METER);

    palette[0].red = palette[0].green = palette[0].blue = 0x00;
    palette[1].red = palette[1].green = palette[1].blue = 0xff;
    png_set_PLTE(png_ptr, info_ptr, 
                 palette, 2);

    trans_alpha[0] = 0xff;
    trans_alpha[1] = 0x00;

    if(transparent)
        png_set_tRNS(png_ptr, info_ptr, trans_alpha, 2, 0);

    png_write_info(png_ptr, info_ptr);

    /* top margin */
    memset(row, 0xff, (realwidth + 7) / 8);
    for(y=0; y<margin * size; y++) {
        png_write_row(png_ptr, row);
    }

    /* data */
    p = qrcode->data;
    for(y=0; y<qrcode->width; y++) {
        bit = 7;
        memset(row, 0xff, (realwidth + 7) / 8);
        q = row;
        q += margin * size / 8;
        bit = 7 - (margin * size % 8);
        for(x=0; x<qrcode->width; x++) {
            for(xx=0; xx<size; xx++) {
                *q ^= (*p & 1) << bit;
                bit--;
                if(bit < 0) {
                    q++;
                    bit = 7;
                }
            }
            p++;
        }
        for(yy=0; yy<size; yy++) {
            png_write_row(png_ptr, row);
        }
    }
    /* bottom margin */
    memset(row, 0xff, (realwidth + 7) / 8);
    for(y=0; y<margin * size; y++) {
        png_write_row(png_ptr, row);
    }

    png_write_end(png_ptr, info_ptr);
    png_destroy_write_struct(&png_ptr, &info_ptr);

    fclose(fp);
    free(row);

    return 0;
}

// GIF rendering
// TODO might add option for rendering in interlaced mode?
static int writeGIF(QRcode *qrcode, const char *outfile) {
    static FILE *fp;
    gdImagePtr gif_ptr;
    int x, y, xx, yy;
    unsigned char *p;
    int colors[2];
    int realwidth;

    realwidth = (qrcode->width + margin * 2) * size;

    fp = openFile(outfile);

    gif_ptr = gdImageCreate(realwidth, realwidth);
    colors[0] = gdImageColorAllocate(gif_ptr, 0xff, 0xff, 0xff);
    colors[1] = gdImageColorAllocate(gif_ptr, 0x00, 0x00, 0x00);

    if(transparent)
        gdImageColorTransparent(gif_ptr, colors[0]);

//  XXX was that for?
//  y = margin * size;
    p = qrcode->data;

    for(y = 0; y < qrcode->width; y++) {
        for(yy = 0; yy < size; yy++) {
            for(x = 0; x < qrcode->width; x++) {
                if(*p & 1) {
                    for(xx = 0; xx < size; xx++) {
                        gdImageSetPixel(gif_ptr,
                                        x * size + margin * size + xx, // resulting x coord
                                        y * size + margin * size + yy, // resulting y coord
                                        colors[1]);
                    }
                }
                p++;
            }
            p = p - qrcode->width;
        }
        p = p + qrcode->width;
    }

    gdImageGif(gif_ptr, fp);

    fclose(fp);
    gdImageDestroy(gif_ptr);

    return 0;
}

// SVG rendering
// XXX This is the most basic type of SVG rendering!
static int writeSVG(QRcode *qrcode, const char *outfile) {
    static FILE *fp;
    int bar;
    int x, y;
    int blocklength;
    unsigned char *p;
    int realwidth;

    realwidth = (qrcode->width + margin * 2) * size;

    fp = openFile(outfile);

    fprintf(fp,
            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n"
            "<svg version=\"1.0\" width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\">\n"
            "  <style>\n"
            "    rect {fill: #000;}\n"
            "  </style>\n",
            realwidth, realwidth);

    if(!transparent)
        fprintf(fp, "  <rect width=\"100%%\" height=\"100%%\" style=\"fill: #fff\" />\n");

    p = qrcode->data;
    for(y = 0; y < qrcode->width; y++) {
        blocklength = 1;
        bar = 0;

        for(x = 0; x < qrcode->width; x++) {
            if(!bar && (*p & 1)) {
                fprintf(fp, "  <rect x=\"%d\" y=\"%d\" ", (x + margin) * size, (y + margin) * size);
                bar = 1;
                blocklength = 1;
            } else if(bar && (*p & 1)) {
                blocklength++; 
            } else if(bar && !(*p & 1)) {
                fprintf(fp, "width=\"%d\" height=\"%d\" />\n", size * blocklength, size);
                bar = 0;
            }

            p++;
        }
        
        if(bar)
            fprintf(fp, "width=\"%d\" height=\"%d\" />\n", size * blocklength, size);
    }

    fprintf(fp, "</svg>\n");

    fclose(fp);

    return 0;
}

// PDF rendering
static int writePDF(QRcode *qrcode, const char *outfile) {
    static FILE *fp;
    cairo_surface_t *surface;
    double realwidth;
    int xs; /* brick x start coord */

    realwidth = (double)(qrcode->width + margin * 2) * size;

    fp = openFile(outfile);

    surface = cairo_pdf_surface_create_for_stream((cairo_write_func_t)&cairowrite, fp, realwidth, realwidth);

    cairodraw(qrcode, surface, realwidth);

    cairo_surface_flush(surface);
    cairo_surface_destroy(surface);
    fclose(fp);

    return 0;
}

// PS/EPS rendering
static int writePS(QRcode *qrcode, const char *outfile) {
    static FILE *fp;
    cairo_surface_t *surface;
    double realwidth;
    int xs; /* brick x start coord */

    realwidth = (double)(qrcode->width + margin * 2) * size;

    fp = openFile(outfile);

    surface = cairo_ps_surface_create_for_stream((cairo_write_func_t)&cairowrite, fp, realwidth, realwidth);

    // only difference when redering EPS instead of PS
    cairo_ps_surface_set_eps(surface, eps);

    cairodraw(qrcode, surface, realwidth);

    cairo_surface_flush(surface);
    cairo_surface_destroy(surface);
    fclose(fp);

    return 0;
}

// X rendering
static int writeX(QRcode *qrcode, const char *outfile) {
    Display *display;
    Window rootwin;
    Window win;
    int screen;
    XEvent e;
    Atom wmDeleteMessage, wmSizeProperty;
    XSizeHints *sizeHints;
    cairo_surface_t *surface;
    double realwidth;


    realwidth = (double)(qrcode->width + margin * 2) * size;

    display = XOpenDisplay(NULL);
    if(!display) {
        fprintf(stderr, "Failed to open display\n");
        exit(EXIT_FAILURE);
    }

    screen = DefaultScreen(display);
    rootwin = RootWindow(display, screen);

    win = XCreateSimpleWindow(display, rootwin, 1, 1, realwidth, realwidth, 0,
//            BlackPixel(display, screen), BlackPixel(display, screen));
    0x40404040, 0x40404040);

    XStoreName(display, win, "QR Code");
    XSelectInput(display, win, ExposureMask | ButtonPressMask | StructureNotifyMask);
    XMapWindow(display, win);

    sizeHints = XAllocSizeHints();
    sizeHints->flags = PAspect;
    sizeHints->min_aspect.x = 1;
    sizeHints->min_aspect.y = 1;
    sizeHints->max_aspect.x = 1;
    sizeHints->max_aspect.y = 1;

    wmSizeProperty = XInternAtom(display, "WM_SIZE_HINTS", 0);
    XSetWMSizeHints(display, win, sizeHints, wmSizeProperty);
    XSetWMNormalHints(display, win, sizeHints);

    wmDeleteMessage = XInternAtom(display, "WM_DELETE_WINDOW", 0);
    XSetWMProtocols(display, win, &wmDeleteMessage, 1);

    surface = cairo_xlib_surface_create(display, win, DefaultVisual(display, 0), realwidth, realwidth);

    //cairodraw(qrcode, surface, realwidth);

    while(1) {
        XNextEvent(display, &e);
        if(e.type == Expose && e.xexpose.count < 1) {
            cairodraw(qrcode, surface, realwidth);
        } else if(e.type == ButtonPress) {
            //break;
            printf("klicky!\n");
        } else if(e.type == ClientMessage && e.xclient.data.l[0] == wmDeleteMessage) {
            printf("Window Closed\n");
            break;
        } else {
            printf("unknown event %d\n", e.type);
        }
    }

    cairo_surface_flush(surface);
    cairo_surface_destroy(surface);

    XDestroyWindow(display ,win);
    XCloseDisplay(display);	

    return 0;
}

// ASCII "rendering"
// spew out basic ASCII art
static int writeASCII(QRcode *qrcode, const char *outfile) {
    static FILE *fp;
    int x, y;
    char b = '#';
    char w = '.';
    unsigned char *row, *p;
    int realwidth;

    realwidth = (qrcode->width + margin * 2);
    row = (unsigned char *)malloc(realwidth + 2);
    if(row == NULL) {
        fprintf(stderr, "Failed to allocate memory.\n");
        exit(EXIT_FAILURE);
    }

    if(transparent)
        w = ' ';

    fp = openFile(outfile);

    row[realwidth] = '\n';
    row[realwidth +1] = '\0';

    /* top margin */
    memset(row, w, realwidth);
    for(y = 0; y < margin; y++) {
        fprintf(fp, "%s", row);
    }

    /* data */
    p = qrcode->data;
    for(y = 0; y < qrcode->width; y++){
        for(x = 0; x < qrcode->width; x++) {
            row[margin + x] = (*p & 1) ? b : w;
            p++;
        }
        fprintf(fp, "%s", row);
    }

    /* bottom margin */
    memset(row, w, realwidth);
    for(y = 0; y < margin; y++) {
        fprintf(fp, "%s", row);
    }

    fclose(fp);
    free(row);

    return 0;
}

/*
 * QRcode member functions
 */

// encoding switch
static QRcode *encode(const char *intext, int length) {
    QRcode *code;

    if(micro) {
        if(eightbit) {
            code = QRcode_encodeDataMQR(length, (unsigned char const *)intext, version, level);
        } else {
            code = QRcode_encodeStringMQR(intext, version, level, hint, casesensitive);
        }
    } else {
        if(eightbit) {
            code = QRcode_encodeData(length, (unsigned char const *)intext, version, level);
        } else {
            code = QRcode_encodeString(intext, version, level, hint, casesensitive);
        }
    }

    return code;
}

// main encoding function
static void qrencode(const char *intext, int length, const char *outfile) {
    QRcode *qrcode;

    qrcode = encode(intext, length);
    if(qrcode == NULL) {
        perror("Failed to encode the input data:");
        exit(EXIT_FAILURE);
    }
    (*writeF)(qrcode, outfile);
    QRcode_free(qrcode);
}

// structured encoding splitter
static QRcode_List *encodeStructured(const char *intext, int length) {
    QRcode_List *list;

    if(eightbit) {
        list = QRcode_encodeDataStructured(length, (unsigned char const *)intext, version, level);
    } else {
        list = QRcode_encodeStringStructured(intext, version, level, hint, casesensitive);
    }

    return list;
}

// main encoding function for structured QR Codes
static void qrencodeStructured(const char *intext, int length, const char *outfile) {
    QRcode_List *qrlist, *p;
    char filename[FILENAME_MAX];
    char *base, *q, *suffix = NULL;
    int i = 1;

    base = strdup(outfile);
    if(base == NULL) {
        fprintf(stderr, "Failed to allocate memory.\n");
        exit(EXIT_FAILURE);
    }

    q = strrchr(base, '.');
    if(q != NULL) {
        suffix = strdup(q);
        *q = '\0';
    } else {
        suffix = "";
    }

    qrlist = encodeStructured(intext, length);
    if(qrlist == NULL) {
        perror("Failed to encode the input data:");
        exit(EXIT_FAILURE);
    }

    for(p = qrlist; p != NULL; p = p->next) {
        if(p->code == NULL) {
            fprintf(stderr, "Failed to encode the input data.\n");
            exit(EXIT_FAILURE);
        }
        if(suffix) {
            snprintf(filename, FILENAME_MAX, "%s-%02d%s", base, i, suffix);
        } else {
            snprintf(filename, FILENAME_MAX, "%s-%02d", base, i);
        }
        (*writeF)(p->code, filename);
        i++;
    }

    free(base);
    if(suffix) {
        free(suffix);
    }

    QRcode_List_free(qrlist);
}

int main(int argc, char **argv) {
    int opt, lindex = -1;
    char *outfile = NULL;
    char *intext = NULL;
    int length = 0;

    while((opt = getopt_long(argc, argv, optstring, options, &lindex)) != -1) {
        switch(opt) {
            case 'h':
                if(lindex == 0) {
                    usage(1, 1);
                } else {
                    usage(1, 0);
                }
                exit(0);
                break;
            case 'o':
                outfile = optarg;
                break;
            case 'f':
                if(!strcasecmp(optarg, "png")) {
                    writeF = &writePNG;
                } else if(!strcasecmp(optarg, "gif")) {
                    writeF = &writeGIF;
                } else if(!strcasecmp(optarg, "svg")) {
                    writeF = &writeSVG;
                } else if(!strcasecmp(optarg, "pdf")) {
                    writeF = &writePDF;
                } else if(!strcasecmp(optarg, "ps")) {
                    writeF = &writePS;
                } else if(!strcasecmp(optarg, "eps")) {
                    eps = 1;
                    writeF = &writePS;
                } else if(!strcasecmp(optarg, "ascii")) {
                    writeF = &writeASCII;
                } else if(!strcasecmp(optarg, "x")) {
                    writeF = &writeX;
                } else {
                    fprintf(stderr, "Invalid format: %s\n", optarg);
                    exit(EXIT_FAILURE);
                }
                break;
            case 's':
                size = atoi(optarg);
                if(size <= 0) {
                    fprintf(stderr, "Invalid size: %d\n", size);
                    exit(EXIT_FAILURE);
                }
                break;
            case 'd':
                dpi = atoi(optarg);
                if(dpi < 0) {
                    fprintf(stderr, "Invalid DPI: %d\n", dpi);
                    exit(EXIT_FAILURE);
                }
                break;
            case 't':
                transparent = 1;
                break;
            case 'M':
                micro = 1;
                break;
            case 'v':
                version = atoi(optarg);
                if(version < 0) {
                    fprintf(stderr, "Invalid version: %d\n", version);
                    exit(EXIT_FAILURE);
                }
                break;
            case 'l':
                switch(*optarg) {
                    case 'l':
                    case 'L':
                        level = QR_ECLEVEL_L;
                        break;
                    case 'm':
                    case 'M':
                        level = QR_ECLEVEL_M;
                        break;
                    case 'q':
                    case 'Q':
                        level = QR_ECLEVEL_Q;
                        break;
                    case 'h':
                    case 'H':
                        level = QR_ECLEVEL_H;
                        break;
                    default:
                        fprintf(stderr, "Invalid level: %s\n", optarg);
                        exit(EXIT_FAILURE);
                        break;
                }
                break;
            case 'm':
                margin = atoi(optarg);
                if(margin < 0) {
                    fprintf(stderr, "Invalid margin: %d\n", margin);
                    exit(EXIT_FAILURE);
                }
                break;
            case 'S':
                structured = 1;
            case 'k':
                hint = QR_MODE_KANJI;
                break;
            case 'c':
                casesensitive = 1;
                break;
            case 'i':
                casesensitive = 0;
                break;
            case '8':
                eightbit = 1;
                break;
            case 'V':
                usage(0, 0);
                exit(0);
                break;
            default:
                fprintf(stderr, "Try `qrencode --help' for more information.\n");
                exit(EXIT_FAILURE);
                break;
        }
    }

    if(argc == 1) {
        usage(1, 0);
        exit(0);
    }

    if(outfile == NULL) {
//        fprintf(stderr, "No output filename is given.\n");
//        exit(EXIT_FAILURE);
        outfile = "-";
    }

    if(optind < argc) {
        intext = argv[optind];
        length = strlen(intext);
    }
    if(intext == NULL) {
        intext = readStdin(&length);
    }

    if(micro && version > MQRSPEC_VERSION_MAX) {
        fprintf(stderr, "Version must be less or equal to %d.\n", MQRSPEC_VERSION_MAX);
        exit(EXIT_FAILURE);
    } else if(!micro && version > QRSPEC_VERSION_MAX) {
        fprintf(stderr, "Version must be less or equal to %d.\n", QRSPEC_VERSION_MAX);
        exit(EXIT_FAILURE);
    }

    if(margin < 0) {
        if(micro)
            margin = 2;
        else
            margin = 4;
    }

    if(micro) {
        if(version == 0) {
            fprintf(stderr, "Version must be specified for Micro QR Code.\n");
            exit(EXIT_FAILURE);
        }
        if(structured) {
            fprintf(stderr, "Micro QR Code does not support structured symbols.\n");
            exit(EXIT_FAILURE);
        }
    }

    if(structured) {
        if(version == 0) {
            fprintf(stderr, "Version must be specified to encode structured symbols.\n");
            exit(EXIT_FAILURE);
        }
        qrencodeStructured(intext, length, outfile);
    } else {
        qrencode(intext, length, outfile);
    }

    return 0;
}
