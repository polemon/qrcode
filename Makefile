# project settings
EXEC=qrcode
SRC=$(wildcard *.c)
OBJ=$(SRC:.c=.o)

# compiler / linker settings
CC=clang
CFLAGS=`pkg-config --cflags libpng gdlib cairo x11`
LDFLAGS=`pkg-config  --libs libpng gdlib cairo x11` -lqrencode

.PHONY: all clean

# build
all: $(EXEC)

# main target
$(EXEC): $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $?

# compile objects
%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<

# clean up
clean:
	-rm -f $(EXEC) $(OBJ)
